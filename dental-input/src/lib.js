import DentalInput from "./components/DentalInput.vue";
import SvgInput from "./components/SvgInput.vue";
import filters from "./teethFilters";

export { DentalInput, SvgInput, filters };
