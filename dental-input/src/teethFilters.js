function uns(tooth) {
  return {
    tooth1: 1,
    tooth2: 2,
    tooth3: 3,
    tooth4: 4,
    tooth5: 5,
    tooth6: 6,
    tooth7: 7,
    tooth8: 8,
    tooth9: 9,
    tooth10: 10,
    tooth11: 11,
    tooth12: 12,
    tooth13: 13,
    tooth14: 14,
    tooth15: 15,
    tooth16: 16,
    tooth17: 17,
    tooth18: 18,
    tooth19: 19,
    tooth20: 20,
    tooth21: 21,
    tooth22: 22,
    tooth23: 23,
    tooth24: 24,
    tooth25: 25,
    tooth26: 26,
    tooth27: 27,
    tooth28: 28,
    tooth29: 29,
    tooth30: 30,
    tooth31: 31,
    tooth32: 32,
  }[tooth];
}

function palmer(tooth) {
  return {
    tooth1: "8┘",
    tooth2: "7┘",
    tooth3: "6┘",
    tooth4: "5┘",
    tooth5: "4┘",
    tooth6: "3┘",
    tooth7: "2┘",
    tooth8: "1┘",
    tooth9: "└1",
    tooth10: "└2",
    tooth11: "└3",
    tooth12: "└4",
    tooth13: "└5",
    tooth14: "└6",
    tooth15: "└7",
    tooth16: "└8",
    tooth17: "┌8",
    tooth18: "┌7",
    tooth19: "┌6",
    tooth20: "┌5",
    tooth21: "┌4",
    tooth22: "┌3",
    tooth23: "┌2",
    tooth24: "┌1",
    tooth25: "1┐",
    tooth26: "2┐",
    tooth27: "3┐",
    tooth28: "4┐",
    tooth29: "5┐",
    tooth30: "6┐",
    tooth31: "7┐",
    tooth32: "8┐",
  }[tooth];
}

function combinedPalmer(set) {
  let palmerArray = Array.from(set).map((tooth) => palmer(tooth));
  let combinedStr = "";
  // noinspection NonAsciiCharacters
  let combinedObject = {
    "┘": palmerArray
      .filter((item) => item.includes("┘"))
      .map((item) => item.replace("┘", "")),
    "└": palmerArray
      .filter((item) => item.includes("└"))
      .map((item) => item.replace("└", "")),
    "┐": palmerArray
      .filter((item) => item.includes("┐"))
      .map((item) => item.replace("┐", "")),
    "┌": palmerArray
      .filter((item) => item.includes("┌"))
      .map((item) => item.replace("┌", "")),
  };

  if (combinedObject["┘"].length) {
    combinedStr += combinedObject["┘"].sort().reverse().join("") + "┘";
  }
  if (combinedObject["└"].length) {
    combinedStr += "└" + combinedObject["└"].sort().join("");
  }

  if (combinedObject["┐"].length + combinedObject["┌"].length > 0) {
    combinedStr += " ";
  }

  if (combinedObject["┐"].length) {
    combinedStr += combinedObject["┐"].sort().reverse().join("") + "┐";
  }
  if (combinedObject["┌"].length) {
    combinedStr += "┌" + combinedObject["┌"].sort().join("");
  }

  return combinedStr;
}

function iso3950(tooth) {
  return {
    tooth1: 18,
    tooth2: 17,
    tooth3: 16,
    tooth4: 15,
    tooth5: 14,
    tooth6: 13,
    tooth7: 12,
    tooth8: 11,
    tooth9: 21,
    tooth10: 22,
    tooth11: 23,
    tooth12: 24,
    tooth13: 25,
    tooth14: 26,
    tooth15: 27,
    tooth16: 28,
    tooth17: 38,
    tooth18: 37,
    tooth19: 36,
    tooth20: 35,
    tooth21: 34,
    tooth22: 33,
    tooth23: 32,
    tooth24: 31,
    tooth25: 41,
    tooth26: 42,
    tooth27: 43,
    tooth28: 44,
    tooth29: 45,
    tooth30: 46,
    tooth31: 47,
    tooth32: 48,
  }[tooth];
}

function alphanumerical(tooth) {
  return {
    tooth1: "UR8",
    tooth2: "UR7",
    tooth3: "UR6",
    tooth4: "UR5",
    tooth5: "UR4",
    tooth6: "UR3",
    tooth7: "UR2",
    tooth8: "UR1",
    tooth9: "UL1",
    tooth10: "UL2",
    tooth11: "UL3",
    tooth12: "UL4",
    tooth13: "UL5",
    tooth14: "UL6",
    tooth15: "UL7",
    tooth16: "UL8",
    tooth17: "LL8",
    tooth18: "LL7",
    tooth19: "LL6",
    tooth20: "LL5",
    tooth21: "LL4",
    tooth22: "LL3",
    tooth23: "LL2",
    tooth24: "LL1",
    tooth25: "LR1",
    tooth26: "LR2",
    tooth27: "LR3",
    tooth28: "LR4",
    tooth29: "LR5",
    tooth30: "LR6",
    tooth31: "LR7",
    tooth32: "LR8",
  }[tooth];
}

function paleoanthropology(tooth) {
  return {
    tooth1: "RM³",
    tooth2: "RM²",
    tooth3: "RM¹",
    tooth4: "RP⁴",
    tooth5: "RP³",
    tooth6: "RC⁻",
    tooth7: "RI²",
    tooth8: "RI¹",
    tooth9: "LI¹",
    tooth10: "LI²",
    tooth11: "LC⁻",
    tooth12: "LP³",
    tooth13: "LP⁴",
    tooth14: "LM¹",
    tooth15: "LM²",
    tooth16: "LM³",
    tooth17: "LM₃",
    tooth18: "LM₂",
    tooth19: "LM₁",
    tooth20: "LP₄",
    tooth21: "LP₃",
    tooth22: "LC₋",
    tooth23: "LI₂",
    tooth24: "LI₁",
    tooth25: "RI₁",
    tooth26: "RI₂",
    tooth27: "RC₋",
    tooth28: "RP₃",
    tooth29: "RP₄",
    tooth30: "RM₁",
    tooth31: "RM₂",
    tooth32: "RM₃",
  }[tooth];
}

/**
 * American Dental Association (ada.org)
 * Source: https://www.news-medical.net/health/Universal-Numbering-System-for-Teeth.aspx
 */
function ada(tooth) {
  return {
    tooth1: "3rd Molar commonly known as wisdom tooth",
    tooth2: "2nd Molar",
    tooth3: "1st Molar",
    tooth4: "2nd Bicuspid also known as 2nd premolar",
    tooth5: "1st Bicuspid or 1st premolar",
    tooth6: "Cuspid or canine",
    tooth7: "Lateral incisor (upper right)",
    tooth8: "Central incisor (upper right)",
    tooth9: "Central incisor (upper left)",
    tooth10: "Lateral incisor (upper left)",
    tooth11: "Cuspid (canine/eye tooth)",
    tooth12: "1st Bicuspid or 1st premolar",
    tooth13: "2nd Bicuspid or 2nd premolar",
    tooth14: "1st Molar",
    tooth15: "2nd Molar",
    tooth16: "3rd Molar or wisdom tooth",
    tooth17: "3rd Molar or wisdom tooth (lower left )",
    tooth18: "2nd Molar",
    tooth19: "1st Molar",
    tooth20: "2nd Bicuspid or 2nd premolar",
    tooth21: "1st Bicuspid or 1st premolar",
    tooth22: "Cuspid or canine",
    tooth23: "Lateral incisor",
    tooth24: "Central incisor",
    tooth25: "Central incisor",
    tooth26: "Lateral incisor",
    tooth27: "Cuspid or canine",
    tooth28: "1st Bicuspid or 1st premolar",
    tooth29: "2nd Bicuspid or 2nd premolar",
    tooth30: "1st Molar",
    tooth31: "2nd Molar",
    tooth32: "3rd Molar (lower right wisdom tooth)",
  }[tooth];
}

function type(tooth) {
  return {
    tooth1: "Molar",
    tooth2: "Molar",
    tooth3: "Molar",
    tooth4: "Premolar",
    tooth5: "Premolar",
    tooth6: "Canine",
    tooth7: "Incisor",
    tooth8: "Incisor",
    tooth9: "Incisor",
    tooth10: "Incisor",
    tooth11: "Canine",
    tooth12: "Premolar",
    tooth13: "Premolar",
    tooth14: "Molar",
    tooth15: "Molar",
    tooth16: "Molar",
    tooth17: "Molar",
    tooth18: "Molar",
    tooth19: "Molar",
    tooth20: "Premolar",
    tooth21: "Premolar",
    tooth22: "Canine",
    tooth23: "Incisor",
    tooth24: "Incisor",
    tooth25: "Incisor",
    tooth26: "Incisor",
    tooth27: "Canine",
    tooth28: "Premolar",
    tooth29: "Premolar",
    tooth30: "Molar",
    tooth31: "Molar",
    tooth32: "Molar",
  }[tooth];
}

function region(tooth) {
  return {
    tooth1: "Upper Right",
    tooth2: "Upper Right",
    tooth3: "Upper Right",
    tooth4: "Upper Right",
    tooth5: "Upper Right",
    tooth6: "Upper Right",
    tooth7: "Upper Right",
    tooth8: "Upper Right",
    tooth9: "Upper Left",
    tooth10: "Upper Left",
    tooth11: "Upper Left",
    tooth12: "Upper Left",
    tooth13: "Upper Left",
    tooth14: "Upper Left",
    tooth15: "Upper Left",
    tooth16: "Upper Left",
    tooth17: "Lower Left",
    tooth18: "Lower Left",
    tooth19: "Lower Left",
    tooth20: "Lower Left",
    tooth21: "Lower Left",
    tooth22: "Lower Left",
    tooth23: "Lower Left",
    tooth24: "Lower Left",
    tooth25: "Lower Right",
    tooth26: "Lower Right",
    tooth27: "Lower Right",
    tooth28: "Lower Right",
    tooth29: "Lower Right",
    tooth30: "Lower Right",
    tooth31: "Lower Right",
    tooth32: "Lower Right",
  }[tooth];
}

export default {
  uns: uns,
  palmer: palmer,
  combinedPalmer: combinedPalmer,
  fdi: iso3950,
  alphanum: alphanumerical,
  paleoanthropology: paleoanthropology,
  ada: ada,
  type: type,
  region: region,
};
